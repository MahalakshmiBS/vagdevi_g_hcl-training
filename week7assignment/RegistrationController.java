package com.assignment.books.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.assignment.books.bean.Login;
import com.assignment.books.bean.Registration;
import com.assignment.books.dao.LoginDao;
import com.assignment.books.dao.RegistrationDao;
import com.assignment.books.dbresource.DbConnection;
import com.assignment.books.service.LoginService;
import com.assignment.books.service.RegisterService;

@WebServlet("/RegistrationController")
public class RegistrationController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RegisterService rs = new RegisterService();
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		PrintWriter pw = response.getWriter();
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		String email=request.getParameter("email");
		Registration reg = new Registration();
		reg.setUname(username);
		reg.setPassword(password);
		reg.setEmail(email);
		
		RegisterService ls = new RegisterService();
		String res = ls.storeRegistration(reg);
						doGet(request, response);				
		pw.println(res);
		RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
		rd.include(request, response);

		
	}


}
	

	
