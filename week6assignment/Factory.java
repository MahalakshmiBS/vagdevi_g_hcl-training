package com.hcl.dao;

public class Factory {
public IMovieDAO getCategory(String category)
{
	if(category == null){
        return null;
     }		
     if(category.equals("movies-coming"))
     {
        return new MoviesComming();
        
     } 
     else if(category.equals("movies-in-theaters"))
     {
        return new MovieInTheater();
        
     } 
     else if(category.equals("top-rated-india"))
     {
        return new TopRatedIndia();
     }
     else if(category.equals("top-rated-movies"))
     {
        return new TopRated();
     }
     return null;
}
}
