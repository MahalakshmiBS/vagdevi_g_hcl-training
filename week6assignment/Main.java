package com.hcl.main;
import java.sql.SQLException;
import java.util.Scanner;
import com.hcl.model.*;
import com.hcl.dao.*;
public class Main {
public static void main(String args[])throws SQLException
{
	Factory f=new Factory();
	Movie m=new Movie();
	Scanner in=new Scanner(System.in);
	while(true) {
		System.out.println("Choose Movie Category :-\n Enter 1 for Movies Comming\n Enter 2 for Movies In Theater\n Enter 3 for Top Rated In India\n Enter 4 for Top Rated Movies\nEnter 5 for Exit\n");
	int c=in.nextInt();
	switch(c)
	{
	case 1:
		IMovieDAO imd=f.getCategory("movies-coming");
		System.out.println("Choose the Option :-\n Enter 1 for Inserting Data\n Enter 2 for Delete Data \n Enter 3 for Displaying Data in Table\n Enter 4 for Updating Data");
		int i=in.nextInt();
		switch(i)
		{
		case 1:
			m.setId(41);
			m.setTitle("Hello");
			m.setYear(2017);
			m.setCategory("movies-coming");
			imd.add(m);
			break;
		case 2:
			System.out.println("Enter Id");
			int j=in.nextInt();
			imd.delete(j);
			break;
		case 3:
			imd.getMovie("movies-coming");
			break;
		case 4:
			m.setId(41);
			m.setTitle("Home");
			m.setYear(2019);
			m.setCategory("movies-coming");
			imd.update(m);
			break;
		default:
			System.out.println("Enter correct input");
		}
		break;
	case 2:
		IMovieDAO imd1=f.getCategory("movies-in-theaters");
		System.out.println("Choose the Option :-\n Enter 1 for Inserting Data\n Enter 2 for Delete Data \n Enter 3 for Displaying Data in Table\n Enter 4 for Updating Data");
		int i1=in.nextInt();
		switch(i1)
		{
		case 1:
			m.setId(42);
			m.setTitle("Hi");
			m.setYear(2018);
			m.setCategory("movies-in-theaters");
			imd1.add(m);
			break;
		case 2:
			System.out.println("Enter Id");
			int j=in.nextInt();
			imd1.delete(j);
			break;
		case 3:
			imd1.getMovie("movies-in-theaters");
			break;
		case 4:
			m.setId(42);
			m.setTitle("Hi updated");
			m.setYear(2018);
			m.setCategory("movies-in-theaters");
			imd1.update(m);
			break;
		default:
			System.out.println("Enter correct input");
		}
		break;
	case 3:
		IMovieDAO imd2=f.getCategory("top-rated-india");
		System.out.println("Choose the Option :-\n Enter 1 for Inserting Data\n Enter 2 for Delete Data \n Enter 3 for Displaying Data in Table\n Enter 4 for Updating Data");
		int i2=in.nextInt();
		switch(i2)
		{
		case 1:
			m.setId(43);
			m.setTitle("How");
			m.setYear(2018);
			m.setCategory("top-rated-india");
			imd2.add(m);
			break;
		case 2:
			System.out.println("Enter Id");
			int j=in.nextInt();
			imd2.delete(j);
			break;
		case 3:
			imd2.getMovie("top-rated-india");
			break;
		case 4:
			m.setId(43);
			m.setTitle("How updated");
			m.setYear(2018);
			m.setCategory("top-rated-india");
			imd2.update(m);
			break;
		default:
			System.out.println("Enter correct input");
		}
		break;
	case 4:
		IMovieDAO imd3=f.getCategory("top-rated-movies");
		System.out.println("Choose the Option :-\n Enter 1 for Inserting Data\n Enter 2 for Delete Data \n Enter 3 for Displaying Data in Table\n Enter 4 for Updating Data");
		int i3=in.nextInt();
		switch(i3)
		{
		case 1:
			m.setId(44);
			m.setTitle("How are you");
			m.setYear(2017);
			m.setCategory("top-rated-movies");
			imd3.add(m);
			break;
		case 2:
			System.out.println("Enter Id");
			int j=in.nextInt();
			imd3.delete(j);
			break;
		case 3:
			imd3.getMovie("top-rated-movies");
			break;
		case 4:
			m.setId(44);
			m.setTitle("How are you updated");
			m.setYear(2017);
			m.setCategory("top-rated-movies");
			imd3.update(m);
			break;
		default:
			System.out.println("Enter correct input");
		}
		break;
	case 5:
		System.exit(1);
		break;
	default:
		System.out.println("Enter correct option");
	}
	}
	
}
}
