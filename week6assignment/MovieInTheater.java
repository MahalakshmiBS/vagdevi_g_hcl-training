package com.hcl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.hcl.model.Movie;
import com.hcl.util.DBConnection;

public class MovieInTheater implements IMovieDAO {
	static Connection con
    = DBConnection.getConnection();
	@Override
	public int add(Movie m) throws SQLException {
		// TODO Auto-generated method stub
	String query= "insert into movie(id,"+ "title,"+"year,"+"category) VALUES (?,?,?,?)";
    PreparedStatement ps= con.prepareStatement(query);
    ps.setInt(1, m.getId());
    ps.setString(2, m.getTitle());
    ps.setInt(3, m.getYear());
    ps.setString(4, m.getCategory());
    int n = ps.executeUpdate();
    return n;
	}

	@Override
	public void delete(int id) throws SQLException {
		// TODO Auto-generated method stub
		String category="movies-in-theaters";
		String query
        = "delete from movie where id =?";
    PreparedStatement ps
        = con.prepareStatement(query);
    ps.setInt(1,id);

    ps.executeUpdate();
	}

	@Override
	public Movie getMovie(String category) throws SQLException {
		String query="select * from movie where category=?"; 
		PreparedStatement ps
        = con.prepareStatement(query);
		ps.setString(1, category);
		ResultSet rs=ps.executeQuery();
		while (rs.next()) {
			System.out.println(rs.getInt(1)+" "+rs.getString(2)+" "+rs.getInt(3)+" "+rs.getString(4));
		}
		return null;
	}

	@Override
	public void update(Movie m) throws SQLException {
		// TODO Auto-generated method stub
		String query
        = "update movie set title=?, "
          + " year= ? where id = ? and category=?";
    PreparedStatement ps
        = con.prepareStatement(query);
    ps.setString(1, m.getTitle());
    ps.setInt(2, m.getYear());
    ps.setInt(3, m.getId());
    ps.setString(4, m.getCategory());
    ps.executeUpdate();
	}
}
// TODO Auto-generated method stub