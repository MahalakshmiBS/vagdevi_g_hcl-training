package com;
public class AdminDepartment extends SuperDepartment 
{
	public String deparmentName() 
	{
		return "Admin Department";
	}
	public String getTodaysWork(int c) 
	{
		return new SuperDepartment().getTodaysWork(c);
	}
	public String getWorkDeadline(int c) 
	{
		return new SuperDepartment().getWorkDeadline(c);
	}
	public String isTodayAHoliday(String s)
	{
		return new SuperDepartment().isTodayAHoliday(s);
	}
}


