package com;
public class HrDepartment extends SuperDepartment 
{
	public String deparmentName() 
	{
		return "HR Department";
	}
	public String getTodaysWork(int c) 
	{
		return new SuperDepartment().getTodaysWork(c);
	}
	public String getWorkDeadline(int c) 
	{
		return new SuperDepartment().getWorkDeadline(c);
	}
	public String doActivity()
	{
		return "Team Lunch";
	}
	public String isTodayAHoliday(String s)
	{
		return new SuperDepartment().isTodayAHoliday(s);
	}
}



